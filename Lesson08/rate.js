window.onload = ()=>{

    const addItem = (in_item) => {
        const list = document.getElementById("root");
        const item = document.createElement("li");
        item.innerHTML = `${in_item.txt} ${in_item.cc} ${in_item.rate}`;
        list.appendChild(item);
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            myObj.forEach(item => addItem(item));
        }
    };
    xmlhttp.open("GET", "https://old.bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true);
    xmlhttp.send();
}