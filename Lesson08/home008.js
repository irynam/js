window.onload = ()=>{
    const myNode = document.getElementById("foo");
    const button = document.getElementById("button");

    const N = 10;

    button.onclick = function() {
        (myNode.childElementCount < N) ? addItem() : removeAll();
    };

    let  addItem = () => {
        const item = document.createElement("div");
        item.innerHTML = `Item <b>${myNode.childElementCount}</b>`;
        myNode.appendChild(item);
        console.log(`Add Item ${myNode.childElementCount}`);
    }

    let  removeAll = () => {
        while (myNode.firstChild) {
          myNode.removeChild(myNode.firstChild);
        }
        console.log(`removed all`)
    }
}